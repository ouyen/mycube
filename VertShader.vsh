#version 310 es

// Input vertex data, different for all executions of this shader.
layout(location = 0) in mediump vec3 inVertex;
layout(location = 1) in mediump vec3 inTexColor;
layout(location = 2) in mediump vec3 inNormal;

// Output data ; will be interpolated for each fragment.
out mediump vec3 fragmentColor;
//out mediump vec3 position;

out highp   vec3    transNormal;
out highp   vec3    transPos;
// Values that stay constant for the whole mesh.
uniform mediump mat4 MVP;
uniform mediump mat4 R;

uniform mediump mat4    MV;
uniform mediump mat3    MVIT;

void main(){

	// Output position of the vertex, in clip space : MVP * position
	gl_Position =  MVP * R * vec4(inVertex,1);
	mediump vec3 position = (R * vec4(inVertex,1)).xyz;
	transPos=vec3(MV*vec4(position,1.0));
	transNormal=normalize(MVIT*vec3(R*vec4(inNormal,1.0)));

	fragmentColor = inTexColor;
}

