#include "onecube.h"

OneCube::OneCube(float x, float y, float z) : vbo(0),
                                              px(x), py(y), pz(z), angle(glm::mat4(1.0f)), theta(0) {
}

OneCube::~OneCube(void) {
    if (vbo) gl::DeleteBuffers(1, &vbo);
}

void OneCube::Init(GLuint *Loc) {
    theta = Loc[0];
    _mvp = Loc[1];
    _mv = Loc[2];
    _mvit = Loc[3];

    static char vertices[] = {
            -1, -1, -1, -1, -1, 1, -1, 1, 1,
            -1, -1, -1, -1, 1, 1, -1, 1, -1, // Left

            1, 1, 1, 1, -1, -1, 1, 1, -1,
            1, -1, -1, 1, 1, 1, 1, -1, 1, // Right

            1, 1, -1, -1, -1, -1, -1, 1, -1,
            1, 1, -1, 1, -1, -1, -1, -1, -1, // Back

            -1, 1, 1, -1, -1, 1, 1, -1, 1,
            1, 1, 1, -1, 1, 1, 1, -1, 1, // Front

            1, 1, 1, 1, 1, -1, -1, 1, -1,
            1, 1, 1, -1, 1, -1, -1, 1, 1, // Top

            1, -1, 1, -1, -1, -1, 1, -1, -1,
            1, -1, 1, -1, -1, 1, -1, -1, -1, // Bottom
    };

    GLfloat vbodata[12 * 3 * 9];
    static const GLfloat facecolor[] =
            {
                    0.0f, 1.0f, 0.0f, // Green
                    0.0f, 0.0f, 1.0f, // Blue
                    1.0f, 0.5f, 0.0f, // Oriange
                    1.0f, 0.0f, 0.0f, // Red
                    1.0f, 1.0f, 1.0f, // White
                    1.0f, 1.0f, 0.0f, // Yellow
            };
    static const GLfloat facenormal[] =
            {
                    -1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
            };
    static const float CS = 0.29f;

    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 6; ++j) {
            vbodata[i * 54 + j * 9] = vertices[i * 18 + j * 3] * CS + px;
            vbodata[i * 54 + j * 9 + 1] = vertices[i * 18 + j * 3 + 1] * CS + py;
            vbodata[i * 54 + j * 9 + 2] = vertices[i * 18 + j * 3 + 2] * CS + pz;
            for (int k = 0; k < 3; ++k) {
                vbodata[i * 54 + j * 9 + 3 + k] = facecolor[i * 3 + k];
                vbodata[i * 54 + j * 9 + 6 + k] = facenormal[i * 3 + k];
            }
        }
    }

    gl::GenBuffers(1, &vbo);
    gl::BindBuffer(GL_ARRAY_BUFFER, vbo);
    gl::BufferData(GL_ARRAY_BUFFER, sizeof(vbodata), vbodata, GL_STATIC_DRAW);
}

void OneCube::Render(glm::mat4 mvp, glm::mat4 v) {
    gl::UniformMatrix4fv(theta, 1, GL_FALSE, &angle[0][0]);

    gl::UniformMatrix4fv(_mvp, 1, GL_FALSE, glm::value_ptr(mvp));

    gl::UniformMatrix4fv(_mv, 1, GL_FALSE, glm::value_ptr(v));
    glm::mat3 mvit = glm::inverseTranspose(v);

    gl::UniformMatrix3fv(_mvit, 1, GL_FALSE, glm::value_ptr(mvit));

    // 1rst attribute buffer : vertices
    gl::BindBuffer(GL_ARRAY_BUFFER, vbo);
    gl::EnableVertexAttribArray(0);
    unsigned int _stride = 9 * sizeof(GLfloat);
    gl::VertexAttribPointer(
            0,
            3,
            GL_FLOAT,
            GL_FALSE,
            _stride,
            (void *) 0
    );

    // 2nd attribute buffer : colors
    gl::EnableVertexAttribArray(1);
    gl::VertexAttribPointer(
            1,
            3,
            GL_FLOAT,
            GL_FALSE,
            _stride,
            (void *) (3 * sizeof(GLfloat))
    );

    gl::EnableVertexAttribArray(2);
    gl::VertexAttribPointer(2,
                            3,
                            GL_FLOAT, GL_FALSE,
                            _stride,
                            (void *) (6 * sizeof(GLfloat)));

    // Draw the triangle !
    gl::DrawArrays(GL_TRIANGLES, 0, 12 * 3);
    gl::BindBuffer(GL_ARRAY_BUFFER, 0);
}

void OneCube::AddAngle(const glm::mat4 &a) {
    angle = a * angle;
}

void OneCube::FitAngle(void) {
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            if (angle[i][j] > 0.99) angle[i][j] = 1;
            if (angle[i][j] < -0.99) angle[i][j] = -1;
            if (abs(angle[i][j]) < 0.1) angle[i][j] = 0;
        }
    }
}
