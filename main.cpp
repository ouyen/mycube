#include "PVRShell/PVRShell.h"
#include "PVRUtils/PVRUtilsGles.h"
#include "onecube.h"

/*!*********************************************************************************************************************
 To use the shell, you have to inherit a class from PVRShell
 and implement the five virtual functions which describe how your application initializes, runs and releases the resources.
***********************************************************************************************************************/
void rotate_cube(unsigned char *idx, const unsigned char *face) {
    char tmpcube[2];
    for (int i = 0; i < 2; ++i)
        tmpcube[i] = idx[face[i]];
    for (int i = 0; i < 6; ++i)
        idx[face[i]] = idx[face[i + 2]];
    for (int i = 0; i < 2; ++i)
        idx[face[6 + i]] = tmpcube[i];
}

enum EUniform {
    eR,
    eMVP,
    eMV,
    eMVIT,
    eNumUniforms
};
GLuint uniLoc[eNumUniforms];
const char *sUniformNames[] = {
        "R", "MVP", "MV", "MVIT"
};

class HelloPVR : public pvr::Shell {
    pvr::EglContext _context;
    // UIRenderer used to display text
    pvr::ui::UIRenderer _uiRenderer;
    GLuint _program;
    GLuint _vbo;
    OneCube *cube[26];
    const char cubepos[26 * 3] = {
            -1, 1, -1, 0, 1, -1, 1, 1, -1,
            -1, 1, 0, 0, 1, 0, 1, 1, 0,
            -1, 1, 1, 0, 1, 1, 1, 1, 1,
            -1, 0, -1, 0, 0, -1, 1, 0, -1,
            -1, 0, 0, 1, 0, 0,
            -1, 0, 1, 0, 0, 1, 1, 0, 1,
            -1, -1, -1, 0, -1, -1, 1, -1, -1,
            -1, -1, 0, 0, -1, 0, 1, -1, 0,
            -1, -1, 1, 0, -1, 1, 1, -1, 1,
    };
    unsigned char cubeidx[26] = {
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
    const unsigned char cubeface[6][9] = {
            {0,  1,  2,  5,  8,  7,  6,  3,  4}, // Top
            {23, 24, 25, 22, 19, 18, 17, 20, 21}, // Bottom
            {0,  3,  6,  14, 23, 20, 17, 9,  12}, // Left
            {8,  5,  2,  11, 19, 22, 25, 16, 13}, // Right
            {6,  7,  8,  16, 25, 24, 23, 14, 15}, // Front
            {17, 18, 19, 11, 2,  1,  0,  9,  10}, // Back
    };
    const float speed = glm::radians(3.8f);
//    uint32_t mvpLoc;
//    uint32_t RLoc;
    glm::mat4 _projection, _view, _model, _mvp;
    GLfloat angle = 0;
    int step = 0;
    glm::vec3 dir[6] = {
            glm::vec3(0, 1, 0),
            glm::vec3(0, -1, 0),
            glm::vec3(-1, 0, 0),
            glm::vec3(1, 0, 0),
            glm::vec3(0, 0, 1),
            glm::vec3(0, 0, -1),
    };
    bool rotating = false;
//    int control_keys[8] = {
//            GLFW_KEY_U, GLFW_KEY_D, GLFW_KEY_L, GLFW_KEY_R,
//            GLFW_KEY_F, GLFW_KEY_B, GLFW_KEY_A, GLFW_KEY_W,
//    };
    pvr::Keys control_keys[8] = {
            pvr::Keys::U, pvr::Keys::D, pvr::Keys::L, pvr::Keys::R,
            pvr::Keys::F, pvr::Keys::B, pvr::Keys::A, pvr::Keys::W,
    };
    unsigned char key_idx[4][4] = {
            {4, 3, 5, 2},  // F, R, B, L
            {4, 1, 5, 0},  // F, D, B, U
            {2, 5, 3, 4},  // L, B, R, F
            {0, 5, 1, 4},  // U, B, D, F
    };
    char ccw = 1;
    float _camTheta;
    float _camRho;
    float _camPhi;
public:
    // following function must be override
    virtual pvr::Result initApplication();

    virtual pvr::Result initView();

    virtual pvr::Result renderFrame();

    virtual pvr::Result releaseView();

    virtual pvr::Result quitApplication();
};

/*!*********************************************************************************************************************
\return Return Result::Success if no error occurred
\brief  Code in initApplication() will be called by pvr::Shell once per run, before the rendering context is created.
    Used to initialize variables that are not dependent on it (e.g. external modules, loading meshes, etc.)
    If the rendering context is lost, initApplication() will not be called again.
***********************************************************************************************************************/
pvr::Result HelloPVR::initApplication() {
    _camTheta = glm::radians(45.0f);
    _camRho = 5.0f;
    _camPhi = glm::radians(45.0f);
    return pvr::Result::
    Success;
}

/*!*********************************************************************************************************************
\return Return Result::Success if no error occurred
\brief  Code in quitApplication() will be called by pvr::Shell once per run, just before exiting the program.
        If the rendering context is lost, QuitApplication() will not be called.
***********************************************************************************************************************/
pvr::Result HelloPVR::quitApplication() {
    return pvr::Result::
    Success;
}

/*!*********************************************************************************************************************
\return Return Result::Success if no error occured
\brief  Code in initView() will be called by pvr::Shell upon initialization or after a change in the rendering context.
    Used to initialize variables that are dependant on the rendering context (e.g. textures, vertex buffers, etc.)
***********************************************************************************************************************/
pvr::Result HelloPVR::initView() {
    // Initialize the PowerVR OpenGL bindings. Must be called before using any of the gl:: commands.
    _context = pvr::createEglContext();
    _context->init(getWindow(), getDisplay(), getDisplayAttributes());

    // Setup the text to be rendered
    _uiRenderer.init(getWidth(), getHeight(), isFullScreen(), (_context->getApiVersion() == pvr::Api::OpenGLES2) ||
                                                              (getBackBufferColorspace() == pvr::ColorSpace::sRGB));
    _uiRenderer.getDefaultTitle()->setText("Cube");
    _uiRenderer.getDefaultTitle()->commitUpdates();

    static const char *attribs[] = {"inVertex", "inTexColor", "inNormal"};
    static const uint16_t attribIndices[] = {0, 1, 2};

    _program = pvr::utils::createShaderProgram(*this, "VertShader.vsh",
                                               "FragShader.fsh", attribs, attribIndices, 3, 0, 0);

    for (int i = 0; i < eNumUniforms; ++i) {
        uniLoc[i] = gl::GetUniformLocation(_program, sUniformNames[i]);
    }

    _projection = pvr::math::perspective(pvr::Api::OpenGLES2, 45,
                                         static_cast<float>(this->getWidth()) / static_cast<float>(this->getHeight()),
                                         0.1, 100, 0);
    _model = glm::mat4(1.0f);

    // Sets the clear color
    gl::ClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    gl::Enable(GL_DEPTH_TEST);
    gl::DepthFunc(GL_LESS);


    for (int i = 0; i < 26; ++i) {
        cube[i] = new OneCube(cubepos[i * 3] * 0.6,
                              cubepos[i * 3 + 1] * 0.6,
                              cubepos[i * 3 + 2] * 0.6);
        cube[i]->Init(uniLoc);
    }
//    gl::UniformMatrix4fv(mvpLoc, 1, GL_FALSE, glm::value_ptr(_mvp));
    return pvr::Result::
    Success;
}

/*!*********************************************************************************************************************
\return Return Result::Success if no error occurred
\brief  Code in releaseView() will be called by pvr::Shell when the application quits or before a change in the rendering context.
***********************************************************************************************************************/
pvr::Result HelloPVR::releaseView() {
    // Release Vertex buffer object.
    for (int i = 0; i < 26; ++i)
        delete cube[i];
    if (_vbo)
        gl::DeleteBuffers(1, &_vbo);

    // Frees the OpenGL handles for the program and the 2 shaders
    gl::DeleteProgram(_program);
    return pvr::Result::
    Success;
}

/*!*********************************************************************************************************************
\return Return Result::Success if no error occurred
\brief  Main rendering loop function of the program. The shell will call this function every frame.
***********************************************************************************************************************/
pvr::Result HelloPVR::renderFrame() {
    //  Clears the color buffer. glClear() can also be used to clear the depth or stencil buffer
    //  (GL_DEPTH_BUFFER_BIT or GL_STENCIL_BUFFER_BIT)
    gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gl::UseProgram(_program);
    if (rotating) {
        if (step > 5) { // Rotate the whole cube
            if (angle < glm::radians(90.0f)) {
                angle += speed;
                glm::mat4 rotate_speed = glm::rotate(glm::mat4(1.0f),
                                                     speed * ccw, dir[step - 5]);
                for (int i = 0; i < 26; ++i)
                    cube[i]->AddAngle(rotate_speed);
            } else {
                rotating = false;
                angle = 0;
                for (int i = 0; i < 26; ++i)
                    cube[i]->FitAngle();
                // After rotation, Left becomes Back, ... , etc.
                pvr::Keys tmp = control_keys[key_idx[step - 5 + ccw][0]];
                for (int i = 0; i < 3; ++i)
                    control_keys[key_idx[step - 5 + ccw][i]] =
                            control_keys[key_idx[step - 5 + ccw][i + 1]];
                control_keys[key_idx[step - 5 + ccw][3]] = tmp;
                glm::vec3 tmpdir = dir[key_idx[step - 5 + ccw][0]];
                for (int i = 0; i < 3; ++i)
                    dir[key_idx[step - 5 + ccw][i]] =
                            dir[key_idx[step - 5 + ccw][i + 1]];
                dir[key_idx[step - 5 + ccw][3]] = tmpdir;
            }
        } else {  // if (step > 5)
            if (angle < glm::radians(90.0f)) {
                angle += speed;
                glm::mat4 rotate_speed = glm::rotate(glm::mat4(1.0f),
                                                     speed * ccw, dir[step]);
                for (int i = 0; i < 9; ++i)
                    cube[cubeidx[cubeface[step][i]]]->AddAngle(rotate_speed);
            } else {
                rotating = false;
                angle = 0;
                for (int i = 0; i < 9; ++i)
                    cube[cubeidx[cubeface[step][i]]]->FitAngle();
                rotate_cube(cubeidx, cubeface[step]);
                if (ccw == -1) {
                    rotate_cube(cubeidx, cubeface[step]);
                    rotate_cube(cubeidx, cubeface[step]);
                }
            }
        }
    } else {
        for (int i = 0; i < 8; ++i)
            if (
//              glfwGetKey( window, control_keys[i]) == GLFW_PRESS
                    pvr::Shell::isKeyPressed(control_keys[i])
                    ) {
                step = i;
                rotating = true;
                ccw = 1;  // Counter Clock Wise
                break;
            }
        if (pvr::Shell::isKeyPressed(pvr::Keys::Shift)) {
            ccw = -1;   // Clock Wise
        }
    }

    if (pvr::Shell::isKeyPressed(pvr::Keys::Left))
        _camTheta += 0.05f;
    if (pvr::Shell::isKeyPressed(pvr::Keys::Right))
        _camTheta -= 0.05f;
    if (pvr::Shell::isKeyPressed(pvr::Keys::Up))
        _camPhi += 0.01f;
    if (pvr::Shell::isKeyPressed(pvr::Keys::Down))
        _camPhi -= 0.01f;
    if (pvr::Shell::isKeyPressed(pvr::Keys::Z))
        _camRho += 0.1f;
    if (pvr::Shell::isKeyPressed(pvr::Keys::X))
        _camRho -= 0.1f;
    glm::vec3 _camPosition = glm::vec3(_camRho * cos(_camTheta) * cos(_camPhi),
                                       _camRho * sin(_camPhi),
                                       _camRho * sin(_camTheta) * cos(_camPhi)
    );
    _view = glm::lookAt(
            _camPosition, // Camera position, in World Space
            glm::vec3(0, 0, 0), // and looks at the origin
            glm::vec3(0, 1, 0)  // Head is up
    );
    _mvp = _projection * _view * _model;
    for (int i = 0; i < 26; ++i) {
        cube[i]->Render(_mvp, _view);
    }
    // Display some text
    _uiRenderer.beginRendering();
    _uiRenderer.getDefaultTitle()->render();
    _uiRenderer.endRendering();

    _context->swapBuffers();
    return pvr::Result::
    Success;
}

/// <summary>This function must be implemented by the user of the shell. The user should return its pvr::Shell object defining the behaviour of the application.</summary>
/// <returns>Return a unique ptr to the demo supplied by the user.</returns>
std::unique_ptr<pvr::Shell> pvr::newDemo() {
    return std::unique_ptr<pvr::Shell>(new HelloPVR());
}
