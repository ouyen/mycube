#version 310 es

// Interpolated values from the vertex shaders
in mediump vec3 fragmentColor;
in highp   vec3    transNormal;
in highp   vec3    transPos;
// Ouput data
out mediump vec3 color;

uniform mediump sampler2D mySampler;
const highp float shininess = 20.0;

void main(){

	// Output color = color specified in the vertex shader,
	// interpolated between all 3 surrounding vertices
//    mediump float dist = length(vec3(3.0, 2.0, 2.0) - position);
//    if (fragmentColor.r > 1)
//        color = texture(mySampler, vec2(fragmentColor.g, fragmentColor.b)).rgb;
//    else
    mediump vec3 LightPosition=vec3(0.0f,0.0f,1.0f);
    mediump vec4 LightColor=vec4(1.0f, 1.0f, 1.0f, 1.0f);
    mediump vec3 lightDirection = normalize(LightPosition - transPos);
    highp float brightness = max(dot(transNormal, lightDirection), 0.0)+0.1;
    mediump vec4 diffuse = LightColor * brightness;
    mediump vec4 specular;
    if (brightness > 0.0) {
        highp vec3 eyeDirection = normalize(-transPos);
        highp vec3 halfVector = normalize(lightDirection + eyeDirection);
        highp float spec = pow(max(dot(transNormal, halfVector), 0.0), shininess);
        specular = spec * LightColor;
    } else {
        specular = vec4(0.0);
    }
    color =vec3(vec4(fragmentColor,1.0)*diffuse +specular);
//    color=fragmentColor;
//	color = color * 10 / dist / dist;
}
