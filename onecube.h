#pragma once

#include "PVRShell/PVRShell.h"
#include "PVRUtils/PVRUtilsGles.h"

class OneCube {
private:
    GLuint vbo;
    float px, py, pz;
    glm::mat4 angle;
    GLuint theta;

    // MVP Matrix location
    uint32_t _mvp;

    uint32_t _mv;
    uint32_t _mvit;

public:
    OneCube(float x, float y, float z);

    ~OneCube(void);

    void Init(GLuint *Loc);

    void Render(glm::mat4 mvp, glm::mat4 v);

    void AddAngle(const glm::mat4 &a);

    void FitAngle(void);
};
